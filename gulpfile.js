//modules
 const gulp = require(`gulp`)
 const sass = require(`gulp-sass`)(require(`sass`))
 const browserSync = require(`browser-sync`)
 const autoprefixer = require(`autoprefixer`)
 const clean = require(`gulp-clean`)
//const cleanCss = require(`gulp-clean-css`)
const concat = require(`gulp-concat`)
const imagemin = require(`gulp-imagemin`)
 //const jsMinify = require(`gulp-js-minify`)
const uglify = require(`gulp-uglify`)
const cssnano = require('cssnano')
const postcss = require('gulp-postcss')
const replace = require('gulp-replace')

//-----------------------------------------------------------------------
const files = {
	scssPath: 'src/scss/**/*.scss',
	jsPath: 'src/js/**/*.js',
	imgPath: 'src/img/**/*.+(png|jpg|jpeg|gif|svg)',
}

//-------------------------------------------------------------------------
function clear() {
	return gulp.src('./dist/*', {
		read: false
	})
		.pipe(clean());
}
exports.clear = clear
//--------------------------------------------

//sass tasks - перетворення смарт css (scss, sass) у звичайний css
function buildStyles() {
	let plugins = [
		autoprefixer({overrideBrowserslist: ['last 1 version']}),
		cssnano()
	];
	return gulp.src(files.scssPath)
		.pipe(sass())
		.pipe(postcss(plugins))
		.pipe(gulp.dest('dist/css'));
				
};
exports.buildStyles = buildStyles
//-----------------------------------------------------------------------

//JS tasks - обєднання js файлів та мініфікація
function buildScripts() {
	return gulp.src(files.jsPath)
		.pipe(concat('scripts.min.js'))
		.pipe(uglify())
		.pipe(gulp.dest('dist'));
}
exports.buildScripts = buildScripts
//-----------------------------------------------------------------------

//оптимізація картинок
function buildImages() {
	return gulp.src(files.imgPath)
		.pipe(imagemin())
		.pipe(gulp.dest("dist/img"))
}

exports.buildImages = buildImages;
//------------------------------------------------------------


//watch task
function watchStyles() {
	gulp.watch(files.scssPath, buildStyles).on('change', browserSync.reload);
};

exports.watchStyles = watchStyles;

function watchScripts() {
	gulp.watch(files.jsPath, buildScripts).on('change', browserSync.reload);
}

exports.watchScripts = watchScripts;
//--------------------------------------------------------------------------


function defaultTask(cb) {
   console.log("print default")
    cb()
}
exports.default = defaultTask

//---------------------------------------------------------
function serve(cb) {
	browserSync.init({
		server: {
			baseDir: "./"
		}
	});
	cb();
}

exports.serve = serve
//------------------------------------------------------------


exports.build = gulp.series(clear, buildStyles, buildScripts, buildImages)
exports.dev = gulp.parallel(watchStyles, watchScripts, serve)


